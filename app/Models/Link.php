<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Link extends Model
{
    use HasFactory;

    public const TOKEN_SIZE = 8;

    protected $appends = ['shortLink', 'isExpired'];

    public function getShortLinkAttribute()
    {
        return route('links.handle', ['token' => $this->token], true);
    }

    public function getIsExpiredAttribute()
    {
        $now = (new \DateTime())->format('Y-m-d H:i:s');
        return $now >= $this->expired_at;
    }
}
