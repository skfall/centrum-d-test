<?php

namespace App\Http\Managers;

use App\Http\Repositories\LinksRepository;
use App\Models\Link;
use Illuminate\Support\Str;

class LinksManager
{
    private LinksRepository $linksRepository;

    public function __construct(LinksRepository $linksRepository){
        $this->linksRepository = $linksRepository;
    }

    public function getLinks(){
        return $this->linksRepository->getLinks();
    }

    public function incrementAttempts(Link $link){
        $link->attempts += 1;
        $link->save();
    }

    public function createLink(string $originalLink, int $maxAttempts, \DateTime $expiryDate): bool|Link {
        $link = new Link();
        $link->original_link = $originalLink;
        $link->max_attempts = $maxAttempts;
        $link->expired_at = $expiryDate->format('Y-m-d H:i');
        $link->token = $this->generateUniqueToken();
        return $link->save() ? $link : false;
    }

    public function deleteLink(Link $link): ?bool {
        return $link->delete();
    }

    private function generateUniqueToken(){
        $token = Str::random(Link::TOKEN_SIZE);
        while($this->linksRepository->checkLinkTokenExists($token)) {
            $token = Str::random(Link::TOKEN_SIZE);
        }
        return $token;
    }
}
