<?php

namespace App\Http\Repositories;

use App\Models\Link;

class LinksRepository {
    public function getLinks() {
        return Link::orderByDesc('id')->get();
    }

    public function checkLinkTokenExists(string $token){
        return Link::where('token', $token)->exists();
    }

    public function getLinkByToken(string $token){
        return Link::where('token', $token)->first();
    }
}
