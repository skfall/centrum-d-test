<?php

namespace App\Http\Controllers;

use App\Http\Managers\LinksManager;
use App\Http\Requests\CreateLinkRequest;
use App\Http\Requests\HandleLinkRequest;
use App\Models\Link;
use Inertia\Inertia;

class LinksController extends Controller
{
    private LinksManager $linksManager;

    public function __construct(LinksManager $linksManager){
        $this->linksManager = $linksManager;
    }

    public function dashboard(){
        return Inertia::render('Dashboard', [
            'links' => $this->linksManager->getLinks()
        ]);
    }

    public function handleLink(HandleLinkRequest $request, string $token){
        $this->linksManager->incrementAttempts($request->link);

        return redirect($request->link->original_link, 301)
            ->header('Cache-Control', 'no-store, no-cache, must-revalidate');

    }

    public function createLink(CreateLinkRequest $request){
        $link = $this->linksManager->createLink(
            $request->originalLink,
            $request->maxAttempts,
            $request->expiry
        );

        return $link ? response()->json($link) : response('', 400);
    }

    public function deleteLink(Link $link){
        $success = $this->linksManager->deleteLink($link);
        return response(null, $success ? 204 : 400);
    }
}
