<?php

namespace App\Http\Requests;

use App\Http\Repositories\LinksRepository;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;

class HandleLinkRequest extends FormRequest
{
    private LinksRepository $linksRepository;

    protected $redirectRoute = 'not_found';

    public function __construct(){
        $this->linksRepository = app(LinksRepository::class);
        parent::__construct();
    }

    protected function prepareForValidation()
    {
        $this->merge([
            'token' => $this->token,
        ]);
    }

    protected function getValidatorInstance(): Validator {
        $this->link = $this->linksRepository->getLinkByToken($this->token);
        return parent::getValidatorInstance();
    }

    public function rules(): array {
        return [
            'token' => 'required|size:8'
        ];
    }

    public function withValidator(Validator $validator): void
    {
        $validator->after(function (Validator $validator): void {
            if($this->link){
                if ($this->link->attempts > 0 && $this->link->attempts >= $this->link->max_attempts) {
                    $validator->errors()->add('max_attempts', 'Max attempts limit reached');
                }
                $now = (new \DateTime())->format('Y-m-d H:i:s');
                if($this->link->expired_at <= $now){
                    $validator->errors()->add('expired_at', 'Link is expired');
                }
            }else{
                $validator->errors()->add('link', 'Link does not exists');
            }
        });
    }
}
