<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Factory as ValidationFactory;

class CreateLinkRequest extends FormRequest {

    public function __construct(
        ValidationFactory $factory,
    )
    {
        $factory->extend(
            'checkDateFormat',
            function ($attribute, $value) {
                return strtotime($value) !== false;
            },
            __('Incorrect date format')
        );
    }

    protected function getValidatorInstance(): Validator {
        $this->expiry = new \DateTime($this->expiryDate);
        return parent::getValidatorInstance();
    }

    public function rules(): array {

        return [
            'originalLink' => 'required|url',
            'maxAttempts' => 'required|integer|min:0',
            'expiryDate' => 'required|checkDateFormat'
        ];
    }
}
