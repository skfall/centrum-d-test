#Centrum-D test project
## Links service

-------
### Instalation guide

1. git clone https://gitlab.com/skfall/centrum-d-test.git
2. cd ./centrum-d-test
3. cp ./.env.example ./.env
4. set database connection credentials
5. composer install
6. php artisan key:generate
7. npm install
8. npm run build
9. php artisan migrate
10. php artisan serve
11. open http://localhost:8000 in your browser
