import { useState, useEffect } from 'react'
import {Button, Form, Toast, ToastContainer} from 'react-bootstrap';
import {Header} from "@/Shared/Header";
import {LinksList} from "@/Shared/LinksList";
import axios from "axios";

const formatDate = (date) => {
    return `${date.getFullYear()}-${("00" + (date.getMonth() + 1)).slice(-2)}-${("00" + date.getDate()).slice(-2)}T${("00" + date.getHours()).slice(-2)}:${("00" + date.getMinutes()).slice(-2)}`
}

export default function Dashboard({links = []}) {
    const date = new Date()
    date.setMinutes(date.getMinutes() + 5)
    const initialDate = formatDate(date)

    date.setHours(date.getHours() + 24)
    const maxDate = formatDate(date)

    const [originalLink, setOriginalLink] = useState('')
    const [maxAttempts, setMaxAttempts] = useState(0)
    const [expiryDate, setExpiryDate] = useState(initialDate)
    const [linksList, setLinksList] = useState(links)
    const [errors, setErrors] = useState([])

    const changeDateHandle = (event) => {
        const value = event.target.value

        const seconds = (new Date(value)).getTime()
        const minSeconds = (new Date(initialDate)).getTime()
        const maxSeconds = (new Date(maxDate)).getTime()

        const finalDate = new Date()

        if(seconds > maxSeconds){
            finalDate.setTime(maxSeconds)
        }else if(seconds < minSeconds){
            finalDate.setTime(minSeconds)
        }else{
            finalDate.setTime(seconds)
        }

        setExpiryDate(formatDate(finalDate))
    }

    const deleteLink = async id => {
        const result = await axios.delete(route('links.delete', {id}))
        if(result.status === 204){
            setLinksList(linksList.filter(item => item.id !== id))
        }
    }

    const createLink = async () => {
        try {
            const localDate = new Date(expiryDate)

            const utcDateString = localDate.toUTCString()

            const result = await axios.post(route('links.create'), {
                originalLink, maxAttempts, expiryDate: utcDateString
            })
            if(result.status === 200){
                setExpiryDate(initialDate)
                setMaxAttempts(0)
                setOriginalLink('')
                setErrors([])
                setLinksList([result.data, ...linksList])
            }
        } catch (error) {
            setErrors(error.response.data.errors)
        }

    }

    return (
        <div>
            <Header />

            <div className="container">
                <div className="row mt-20">
                    <div className="col-6">
                        <h3 className="mb-3">Create new link</h3>
                        <Form>
                            <Form.Group className="mb-3">
                                <Form.Label>Original link</Form.Label>
                                <Form.Control type="text" value={originalLink} onChange={event => setOriginalLink(event.target.value)} placeholder="Enter original link" />
                            </Form.Group>

                            <Form.Group className="mb-3">
                                <Form.Label>Redirects limit (0 - unlimited)</Form.Label>
                                <Form.Control type="number" min={0} value={maxAttempts} onChange={event => setMaxAttempts(event.target.value)} placeholder="Enter max redirect limits" />
                            </Form.Group>

                            <Form.Group className="mb-3">
                                <Form.Label>Expiration date (min: 5m, max: 24h)</Form.Label>
                                <Form.Control value={expiryDate} min={initialDate} max={maxDate} onChange={changeDateHandle} type="datetime-local"/>
                            </Form.Group>

                            <Form.Group className="mb-3">
                                <Button variant="primary" onClick={createLink}>Create short link</Button>
                            </Form.Group>
                        </Form>
                    </div>
                    <div className="col-6">
                        <LinksList linksList={linksList} deleteLink={deleteLink} />
                    </div>
                </div>
            </div>

            <ToastContainer  position={'bottom-end'}>
                {Object.values(errors).map(error => {
                    return (
                        <Toast key={`toast_${error}`} className="m-3" bg="warning">
                            <Toast.Header closeButton={false} >
                                <strong className="me-auto">Validation error</strong>
                            </Toast.Header>
                            <Toast.Body>{error}</Toast.Body>
                        </Toast>
                    )
                })}

            </ToastContainer>


        </div>
    );
}
