import {Container, Navbar} from "react-bootstrap";

export const Header = () => {
    return (
        <Navbar expand="lg" bg="dark" data-bs-theme="dark">
            <Container>
                <Navbar.Brand href="#home">Short Links Service</Navbar.Brand>
            </Container>
        </Navbar>
    )
}
