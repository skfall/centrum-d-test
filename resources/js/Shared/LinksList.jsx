import {Button, ListGroup, Alert, Badge} from "react-bootstrap";

export const LinksList = ({ linksList, deleteLink }) => {
    return (
        <>
            <h3 className="mb-3">Links list</h3>
            <label>&nbsp;</label>
            <ListGroup as="ol" numbered>
                {linksList.length === 0 &&
                    <Alert variant={'dark'}>
                        There are no links yet
                    </Alert>
                }
                {linksList.map(link => (
                    <ListGroup.Item
                        key={link?.id}
                        as="li"
                        className="d-flex justify-content-between align-items-start"
                    >
                        <div className="ms-2 me-auto">
                            {link?.isExpired &&
                                <Badge bg="danger" pill>
                                    Expired
                                </Badge>
                            }
                            {link?.max_attempts > 0 &&
                                <>
                                    &nbsp;
                                    <Badge bg={link?.attempts >= link?.max_attempts ? 'danger' : 'success'} pill>
                                        Limit: {(link?.attempts || 0)}/{link?.max_attempts}
                                    </Badge>
                                </>
                            }
                            <div>
                                <span><strong>Short:</strong></span> <a href={link?.shortLink || '#'} target="_blank">{link?.shortLink}</a>
                            </div>
                            <div className="mb-1">
                                <span><strong>Original:</strong></span> <a href={link?.original_link || '#'} target="_blank">{link?.original_link}</a>
                            </div>
                            <div>
                                <Button onClick={() => deleteLink(link?.id)} variant="danger">Delete</Button>
                            </div>
                        </div>
                    </ListGroup.Item>
                ))}
            </ListGroup>
        </>
    )
}
