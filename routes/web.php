<?php

use App\Http\Controllers\ProfileController;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [\App\Http\Controllers\LinksController::class, 'dashboard'])->name('links.dashboard');
Route::get('/404', function (){
    abort(404);
})->name('not_found');
Route::get('/{token}', [\App\Http\Controllers\LinksController::class, 'handleLink'])->name('links.handle');

Route::group(['prefix' => 'links'], function(){
    Route::post('/', [\App\Http\Controllers\LinksController::class, 'createLink'])->name('links.create');
    Route::delete('/{link}', [\App\Http\Controllers\LinksController::class, 'deleteLink'])->name('links.delete');
});
